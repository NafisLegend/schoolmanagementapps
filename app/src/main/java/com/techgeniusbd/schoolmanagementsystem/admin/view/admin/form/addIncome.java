package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment.all_income_array;
import static com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment.all_school_array;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;

public class addIncome extends AppCompatActivity {

    Toolbar myToolbar;
    final Calendar myCalendar = Calendar.getInstance();
    AppCompatSpinner sp_shift, sp_class, sp_section, sp_school, sp_income, sp_month;
    AppCompatSpinner ac_student_name;
    FloatingActionButton add_expense;
    String $_institute, $_date, $_incomeType, $_amount, $_shift, $_class,
            $_section, $_name, $_month, $_year, $_remarks;
    EditText Income_date, Income_year, Income_remarks, Income_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_income);
        setToolBar();
        Initialization();


        add_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getValues();
                SendValueToServer();
            }
        });
    }

    private void SendValueToServer() {

        new SendDataToServer().execute();

    }

    private class SendDataToServer extends AsyncTask<String, Void, Void> {

        String JsonString;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("institute", $_institute);
                jsonObject.put("date", $_date);
                jsonObject.put("incomeType", $_incomeType);
                jsonObject.put("amount", $_amount);
                jsonObject.put("shift", $_shift);
                jsonObject.put("class", $_class);
                jsonObject.put("section", $_section);
                jsonObject.put("name", $_name);
                jsonObject.put("month", $_month);
                jsonObject.put("year", $_year);
                jsonObject.put("remarks", $_remarks);

                Log.e("JsonObject", "" + jsonObject);


                JsonString = Utility.post(stringHolder.AddExpense, "" + jsonObject);


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void getValues() {


        Log.e("INCOME VALUE ", "**** > " + sp_income.getSelectedItemPosition());
        Log.e("INCOME VALUE  1 ", "**** > " + HomeFragment.all_income_list.size());
        // Log.e("INCOME VALUE  2 ","**** > "+HomeFragment.all_incomeItem_list.);


        $_institute = HomeFragment.all_school_list.get(sp_school.getSelectedItemPosition()).getCompany_id();
        $_date = Income_date.getText().toString();
        $_incomeType = HomeFragment.all_income_list.get(sp_income.getSelectedItemPosition()).getIncome_type_id();
        $_amount = Income_amount.getText().toString();

        $_name = HomeFragment.all_student_list.get(ac_student_name.getSelectedItemPosition()).getStudent_id();
        $_shift = HomeFragment.all_shift_list.get(sp_shift.getSelectedItemPosition()).getShift_id();
        $_class = HomeFragment.all_class_list.get(sp_class.getSelectedItemPosition()).getClass_id();
        $_section = HomeFragment.all_section_list.get(sp_section.getSelectedItemPosition()).getSection_id();

        $_month = "" + sp_month.getSelectedItemPosition();
        $_year = Income_year.getText().toString();
        $_remarks = Income_remarks.getText().toString();

    }

    private void Initialization() {

        sp_shift = (AppCompatSpinner) findViewById(R.id.sp_shift);
        sp_class = (AppCompatSpinner) findViewById(R.id.sp_class);
        sp_section = (AppCompatSpinner) findViewById(R.id.sp_section);

        sp_school = (AppCompatSpinner) findViewById(R.id.sp_school);
        sp_income = (AppCompatSpinner) findViewById(R.id.sp_income_);
        sp_month = (AppCompatSpinner) findViewById(R.id.monthSp);
        ac_student_name = (AppCompatSpinner) findViewById(R.id.ac_student_name);

        add_expense = (FloatingActionButton) findViewById(R.id.fab_add_expense);

        ArrayAdapter<String> name_adapter = new ArrayAdapter(this, android.R.layout.select_dialog_item, all_school_array);
        ac_student_name.setAdapter(name_adapter);


        ArrayAdapter shift_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_shift_array);
        ArrayAdapter class_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_class_array);
        ArrayAdapter section_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_section_array);
        ArrayAdapter school_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, all_school_array);
        ArrayAdapter school_income = new ArrayAdapter(this, android.R.layout.simple_spinner_item, all_income_array);

        shift_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        section_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        school_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        school_income.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        name_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_shift.setAdapter(shift_adapter);
        sp_class.setAdapter(class_adapter);
        sp_section.setAdapter(section_adapter);
        sp_school.setAdapter(school_adapter);
        sp_income.setAdapter(school_income);

        Income_date = (EditText) findViewById(R.id.addIncome_date);
        Income_year = (EditText) findViewById(R.id.addIncome_year);
        Income_remarks = (EditText) findViewById(R.id.addIncome_remarks);
        Income_amount = (EditText) findViewById(R.id.addIncome_income);

        Income_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(addIncome.this, DateListner,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });

        $_institute = HomeFragment.all_school_list.get(0).getCompany_id();
        $_incomeType = HomeFragment.all_income_list.get(0).getIncome_type_id();
        $_shift = HomeFragment.all_shift_list.get(0).getShift_id();
        $_class = HomeFragment.all_class_list.get(0).getClass_id();
        $_section = HomeFragment.all_section_list.get(0).getSection_id();
        $_name = HomeFragment.all_student_list.get(0).getStudent_id();


    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.add_income));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    DatePickerDialog.OnDateSetListener DateListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateBirthDate();
        }
    };

    private void updateBirthDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(myCalendar.getTime());
        Income_date.setText(date);

    }


}

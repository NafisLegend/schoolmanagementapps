package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.IncomeAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addIncome;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

public class incomeList extends AppCompatActivity {

    RecyclerView list_all_income_list;
    private RecyclerView.LayoutManager mLayoutManager;
    IncomeAdapter adapter;
    Toolbar myToolbar;

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_income_list);

        setToolBar();

        mLayoutManager = new LinearLayoutManager(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(incomeList.this, addIncome.class));
            }
        });




        adapter = new IncomeAdapter(HomeFragment.all_incomeItem_list, getApplicationContext(), 3);
        list_all_income_list = (RecyclerView) findViewById(R.id.list_all_income_list);
        list_all_income_list.setLayoutManager(mLayoutManager);
        list_all_income_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.income_list));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        adapter.notifyDataSetChanged();
    }
}

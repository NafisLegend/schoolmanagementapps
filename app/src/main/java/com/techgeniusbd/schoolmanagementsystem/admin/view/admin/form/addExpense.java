package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;

public class addExpense extends AppCompatActivity {

    Toolbar myToolbar;
    AppCompatSpinner sp_expense, sp_school;
    FloatingActionButton add_expense;
    EditText expense_remarks, expense_date, expense_amount;
    String $_institute, $_expenseType, $_date, $_amount, $_remarks;
    final Calendar myCalendar = Calendar.getInstance();
    String institute, expenseItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        setToolBar();
        Initialization();

        add_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getValues();
                sendDataToServer();
                Log.e("Selected Position --> ", "--> " + sp_expense.getSelectedItemPosition());
            }
        });


    }

    private void getValues() {
        // $_institute
        //$_expenseType
        $_date = expense_date.getText().toString();
        $_amount = expense_amount.getText().toString();
        $_remarks = expense_remarks.getText().toString();


        institute = HomeFragment.all_school_list.get(sp_school.getSelectedItemPosition()).getCompany_id();
        expenseItem = HomeFragment.all_expense_list.get(sp_expense.getSelectedItemPosition()).getExpense_type_id();


        Log.e("SCHOOL===>", institute);
    }

    private void sendDataToServer() {

        new SendDataToServer().execute();
    }

    private class SendDataToServer extends AsyncTask<String, Void, Void> {

        String JSonDate;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("institute", institute);
                jsonObject.put("expenseType", expenseItem);
                jsonObject.put("date", $_date);
                jsonObject.put("amount", $_amount);
                jsonObject.put("remarks", $_remarks);

                Log.e("JsonObject", "" + jsonObject);
                JSonDate = Utility.post(stringHolder.AddExpense, "" + jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void Initialization() {
        sp_expense = (AppCompatSpinner) findViewById(R.id.sp_expense);
        sp_school = (AppCompatSpinner) findViewById(R.id.sp_school);

        ArrayAdapter school_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_school_array);
        ArrayAdapter expense_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_expense_array);

        sp_expense.setAdapter(expense_adapter);
        sp_school.setAdapter(school_adapter);

        add_expense = (FloatingActionButton) findViewById(R.id.fab_add_expense);

        expense_remarks = (EditText) findViewById(R.id.add_expense_remarks);
        expense_date = (EditText) findViewById(R.id.add_expense_date);
        expense_amount = (EditText) findViewById(R.id.add_expense_amount);

        institute = HomeFragment.all_school_list.get(0).getCompany_id();
        expenseItem = HomeFragment.all_expense_list.get(0).getExpense_type_id();

        expense_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(addExpense.this, DateListner,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });

    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.add_Expense));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }

    DatePickerDialog.OnDateSetListener DateListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateBirthDate();

        }
    };

    private void updateBirthDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(myCalendar.getTime());
        expense_date.setText(date);

    }

}

package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;

import java.util.ArrayList;


public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<studentModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public StudentAdapter(ArrayList<studentModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_student, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String title = list_of_title.get(position).getStudent_name();
        String roll = list_of_title.get(position).getRoll_no();
        String shift = list_of_title.get(position).getShift_id();
        String gender = list_of_title.get(position).getGender();

        holder.name.setText(title);
        holder.roll.setText("Roll : "+roll);
        holder.shift.setText("Shift : "+shift);
        holder.gender.setText("Gender : "+gender);


    }

    @Override
    public int getItemCount() {
        return list_of_title.size();
    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        StudentAdapter.setRecyclerClick = setRecyclerClick;
    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, roll, shift, gender;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name_of_student);
            roll = (TextView) view.findViewById(R.id.roll_of_student);
            shift = (TextView) view.findViewById(R.id.shit_of_student);
            gender = (TextView) view.findViewById(R.id.gender_of_student);

        }
    }

}

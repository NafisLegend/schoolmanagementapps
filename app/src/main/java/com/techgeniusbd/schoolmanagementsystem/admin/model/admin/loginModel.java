package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class loginModel {


    private String id;
    private String name;
    private String password;
    private String email;
    private String emp_id;
    private String branch_id;
    private String company_id;
    private String user_type;
    private String active;

    private String pkSeparator;
    private String repColumnsSeparator;

    public String getPkSeparator() {
        return pkSeparator;
    }

    public void setPkSeparator(String pkSeparator) {
        this.pkSeparator = pkSeparator;
    }

    public String getRepColumnsSeparator() {
        return repColumnsSeparator;
    }

    public void setRepColumnsSeparator(String repColumnsSeparator) {
        this.repColumnsSeparator = repColumnsSeparator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}

package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class subjectModel {


    private String subject_id;
    private String subject_name;
    private String short_name;
    private String group_id;
    private String active;
    private String sl;
    private String pass_marks_sub;
    private String pass_marks_obj;
    private String pass_marks_pra;
    private String company_id;
    private String total_marks;
    private String pass_marks;
    private String sub_part_id;
    private String optional_status;
    private String classS;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getPass_marks_sub() {
        return pass_marks_sub;
    }

    public void setPass_marks_sub(String pass_marks_sub) {
        this.pass_marks_sub = pass_marks_sub;
    }

    public String getPass_marks_obj() {
        return pass_marks_obj;
    }

    public void setPass_marks_obj(String pass_marks_obj) {
        this.pass_marks_obj = pass_marks_obj;
    }

    public String getPass_marks_pra() {
        return pass_marks_pra;
    }

    public void setPass_marks_pra(String pass_marks_pra) {
        this.pass_marks_pra = pass_marks_pra;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getTotal_marks() {
        return total_marks;
    }

    public void setTotal_marks(String total_marks) {
        this.total_marks = total_marks;
    }

    public String getPass_marks() {
        return pass_marks;
    }

    public void setPass_marks(String pass_marks) {
        this.pass_marks = pass_marks;
    }

    public String getSub_part_id() {
        return sub_part_id;
    }

    public void setSub_part_id(String sub_part_id) {
        this.sub_part_id = sub_part_id;
    }

    public String getOptional_status() {
        return optional_status;
    }

    public void setOptional_status(String optional_status) {
        this.optional_status = optional_status;
    }

    public String getClassS() {
        return classS;
    }

    public void setClassS(String classS) {
        this.classS = classS;
    }
}

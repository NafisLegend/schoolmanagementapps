package com.techgeniusbd.schoolmanagementsystem.admin.view.common;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.Home;

public class Splash extends AppCompatActivity implements Animation.AnimationListener {

    Animation animTogether;
    ImageView imgLogo;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        imgLogo = (ImageView) findViewById(R.id.imglogo);

        // load the animation
        animTogether = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.together);

        // set animation listener
        animTogether.setAnimationListener(this);

        imgLogo.startAnimation(animTogether);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {


        if (prefs.getBoolean("logedIn", false)) {
            startActivity(new Intent(Splash.this, Home.class));
            finish();


        } else {
            startActivity(new Intent(Splash.this, LogInChooser.class));
            finish();
        }


    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

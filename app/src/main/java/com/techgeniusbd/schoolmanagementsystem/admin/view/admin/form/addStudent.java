package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;

public class addStudent extends AppCompatActivity {

    Toolbar myToolbar;
    AppCompatSpinner sp_shift, sp_class, sp_section, sp_group, sp_4t_subject, sp_excluded_subject, sp_profession;
    EditText stdName, stdNameBangla, stdRoll, stdDateofBirth, stdAdmissionDate, stdFatherName,
            stdMothersName, stdGurdiansName, stdProfessionDetails, stdGurdinasPassword, mobileNumber,
            mobileNumberFather, mobileNumberMother, mobileNumberGurdinas, stdEmail, stdPresentAddress,
            stdPermaanentAddress;
    String $_name, $_name_bangla, $_shift, $_class, $_section, $_roll, $_group, $_date_of_birth,
            $_addmissionDate, $_gender, $_religion, $_fathersName, $_mothersName, $_GurdinasName,
            $_profession, $_professionDetails, $_passWord, $_mobileNo, $_mobileNo_father,
            $_mobileNo_Mother, $_mobileNo_Gurdian, $_smsDeliverable, $_Email, $_presendAddress,
            $_permanentAddress;
    RadioGroup genderRadio, stdreligion, smsdeliverable;

    final Calendar myCalendar = Calendar.getInstance();
    FloatingActionButton addStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        setToolBar();

        Initialization();


        addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CollectData();
                SendDataToServer();

            }
        });

    }

    private void SendDataToServer() {
        new SendDataToServer().execute();
    }

    private class SendDataToServer extends AsyncTask<String, Void, Void> {

        String JsonResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", $_name);
                jsonObject.put("name_bangla", $_name_bangla);
                jsonObject.put("shift", $_shift);
                jsonObject.put("class", $_class);
                jsonObject.put("section", $_section);
                jsonObject.put("roll", $_roll);
                jsonObject.put("group", $_group);
                jsonObject.put("dob", $_date_of_birth);
                jsonObject.put("admissionDate", $_addmissionDate);
                jsonObject.put("gender", $_gender);
                jsonObject.put("religion", $_religion);
                jsonObject.put("fatherName", $_fathersName);
                jsonObject.put("motherName", $_mothersName);
                jsonObject.put("gurdianName", $_GurdinasName);
                jsonObject.put("profession", $_profession);
                jsonObject.put("profession", $_professionDetails);
                jsonObject.put("passWord", $_passWord);
                jsonObject.put("mobileNumber", $_mobileNo);
                jsonObject.put("mobileFather", $_mobileNo_father);
                jsonObject.put("mobileMother", $_mobileNo_Mother);
                jsonObject.put("mobileGurdian", $_mobileNo_Gurdian);
                jsonObject.put("sms", $_smsDeliverable);
                jsonObject.put("email", $_Email);
                jsonObject.put("presentAddress", $_presendAddress);
                jsonObject.put("permanentAddress", $_permanentAddress);


                Log.e("JsonObject", "" + jsonObject);
                JsonResponse = Utility.post(stringHolder.AddStudent, "" + jsonObject);


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void CollectData() {


        $_name = stdName.getText().toString();
        $_name_bangla = stdNameBangla.getText().toString();


        $_shift = HomeFragment.all_shift_list.get(sp_shift.getSelectedItemPosition()).getShift_id();
        $_class = HomeFragment.all_class_list.get(sp_class.getSelectedItemPosition()).getClass_id();
        $_section = HomeFragment.all_section_list.get(sp_section.getSelectedItemPosition()).getSection_id();


        $_roll = stdRoll.getText().toString();
        $_date_of_birth = stdDateofBirth.getText().toString();

        $_addmissionDate = stdAdmissionDate.getText().toString();
        //$_gender
        // $_religion
        $_fathersName = stdFatherName.getText().toString();
        $_mothersName = stdMothersName.getText().toString();
        $_GurdinasName = stdGurdiansName.getText().toString();

        $_professionDetails = stdProfessionDetails.getText().toString();
        $_passWord = stdGurdinasPassword.getText().toString();
        $_mobileNo = mobileNumber.getText().toString();
        $_mobileNo_father = mobileNumberFather.getText().toString();
        $_mobileNo_Mother = mobileNumberMother.getText().toString();
        $_mobileNo_Gurdian = mobileNumberGurdinas.getText().toString();
        //  $_smsDeliverable
        $_Email = stdEmail.getText().toString();
        $_presendAddress = stdPermaanentAddress.getText().toString();
        $_permanentAddress = stdPermaanentAddress.getText().toString();


        $_group = HomeFragment.all_group_list.get(sp_group.getSelectedItemPosition()).getGroup_id();
        $_profession = HomeFragment.all_profession_list.get(sp_profession.getSelectedItemPosition()).getProfession_id();


    }

    private void Initialization() {

        sp_shift = (AppCompatSpinner) findViewById(R.id.sp_shift);
        sp_class = (AppCompatSpinner) findViewById(R.id.sp_class);
        sp_section = (AppCompatSpinner) findViewById(R.id.sp_section);
        sp_group = (AppCompatSpinner) findViewById(R.id.sp_group);
        sp_4t_subject = (AppCompatSpinner) findViewById(R.id.sp_4t_subject);
        sp_excluded_subject = (AppCompatSpinner) findViewById(R.id.sp_excluded_subject);
        sp_profession = (AppCompatSpinner) findViewById(R.id.sp_profession);
        addStudent = (FloatingActionButton) findViewById(R.id.fab_student);

        genderRadio = (RadioGroup) findViewById(R.id.genderRadio);
        stdreligion = (RadioGroup) findViewById(R.id.stdreligion);
        smsdeliverable = (RadioGroup) findViewById(R.id.smsdeliverable);


        stdName = (EditText) findViewById(R.id.stdName);
        stdNameBangla = (EditText) findViewById(R.id.stdNameBangla);
        stdRoll = (EditText) findViewById(R.id.stdRoll);
        stdDateofBirth = (EditText) findViewById(R.id.stdDateofBirth);
        stdAdmissionDate = (EditText) findViewById(R.id.stdAdmissionDate);
        stdFatherName = (EditText) findViewById(R.id.stdFatherName);
        stdMothersName = (EditText) findViewById(R.id.stdMothersName);
        stdGurdiansName = (EditText) findViewById(R.id.stdGurdiansName);
        stdProfessionDetails = (EditText) findViewById(R.id.stdProfessionDetails);
        stdGurdinasPassword = (EditText) findViewById(R.id.stdGurdinasPassword);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        mobileNumberFather = (EditText) findViewById(R.id.mobileNumberFather);
        mobileNumberMother = (EditText) findViewById(R.id.mobileNumberMother);
        mobileNumberGurdinas = (EditText) findViewById(R.id.mobileNumberGurdinas);
        stdEmail = (EditText) findViewById(R.id.stdEmail);
        stdPresentAddress = (EditText) findViewById(R.id.stdPresentAddress);
        stdPermaanentAddress = (EditText) findViewById(R.id.stdPermaanentAddress);


        ArrayAdapter shift_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_shift_array);
        ArrayAdapter class_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_class_array);
        ArrayAdapter section_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_section_array);
        ArrayAdapter group_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_group_array);
        ArrayAdapter fourthsubject_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_subject_array);
        ArrayAdapter excluded_subject_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_subject_array);
        ArrayAdapter profession_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_profession_array);

        shift_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        section_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fourthsubject_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        group_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        excluded_subject_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        profession_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_shift.setAdapter(shift_adapter);
        sp_class.setAdapter(class_adapter);
        sp_section.setAdapter(section_adapter);
        sp_group.setAdapter(group_adapter);
        sp_4t_subject.setAdapter(fourthsubject_adapter);
        sp_excluded_subject.setAdapter(excluded_subject_adapter);
        sp_profession.setAdapter(profession_adapter);


        stdAdmissionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(addStudent.this, admissiondateSetListener,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });


        stdDateofBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(addStudent.this, birthdateSetListener,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });


        $_shift = HomeFragment.all_shift_list.get(0).getShift_id();
        $_class = HomeFragment.all_class_list.get(0).getClass_id();
        $_section = HomeFragment.all_section_list.get(0).getSection_id();
        $_group = HomeFragment.all_group_list.get(0).getGroup_id();
        $_profession = HomeFragment.all_profession_list.get(0).getProfession_id();
        $_gender = "0";
        $_religion = "0";
        $_smsDeliverable = "0";

        genderRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedID) {

                if (checkedID == R.id.male) {
                    $_gender = "0";
                } else {
                    $_gender = "1";
                }
            }
        });
        stdreligion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedID) {

                if (checkedID == R.id.islam) {
                    $_religion = "0";
                } else if (checkedID == R.id.hindu) {
                    $_religion = "1";
                } else if (checkedID == R.id.buddhist) {
                    $_religion = "2";
                } else {
                    $_religion = "3";
                }

            }
        });
        smsdeliverable.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedID) {

                if (checkedID == R.id.delivery_yes) {
                    $_smsDeliverable = "0";
                } else {
                    $_smsDeliverable = "1";
                }
            }
        });

    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.add_student));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    DatePickerDialog.OnDateSetListener birthdateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateBirthDate();

        }
    };


    DatePickerDialog.OnDateSetListener admissiondateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateAdmissionhDate();

        }
    };

    private void updateBirthDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(myCalendar.getTime());
        stdDateofBirth.setText(date);

    }

    private void updateAdmissionhDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(myCalendar.getTime());
        stdAdmissionDate.setText(date);

    }

}

package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.classModel;

import java.util.ArrayList;


public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<classModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public ClassAdapter(ArrayList<classModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_class, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String title = list_of_title.get(position).getClass_name();

        holder.classname.setText(title);

    }

    @Override
    public int getItemCount() {
        return list_of_title.size();
    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        ClassAdapter.setRecyclerClick = setRecyclerClick;
    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView classname;

        public MyViewHolder(View view) {
            super(view);
            classname = (TextView) view.findViewById(R.id.classname);

        }
    }

}

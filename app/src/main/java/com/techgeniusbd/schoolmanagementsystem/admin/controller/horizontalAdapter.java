package com.techgeniusbd.schoolmanagementsystem.admin.controller;

/**
 * Created by Nafis on 9/28/16.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.subjectModel;

import java.util.ArrayList;


public class horizontalAdapter extends RecyclerView.Adapter<horizontalAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<subjectModel
            > list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public horizontalAdapter(ArrayList<subjectModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_topuser, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String title = list_of_title.get(position).getSubject_name();

        holder.textViewSlider.setText(title);


        if (selected_position == position) {

            // Here I am just highlighting the background
            holder.background.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shape_pressed));
            holder.textViewSlider.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
            holder.background.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shape));
            holder.textViewSlider.setTextColor(context.getResources().getColor(R.color.textcolor));

        }

        holder.textViewSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setRecyclerClick.OnItemClick(position, view);

                notifyItemChanged(selected_position);
                selected_position = position;
                notifyItemChanged(selected_position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list_of_title.size();
    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        horizontalAdapter.setRecyclerClick = setRecyclerClick;


    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewSlider;
        LinearLayout background;

        public MyViewHolder(View view) {
            super(view);
            textViewSlider = (TextView) view.findViewById(R.id.textViewSlider);
            background=(LinearLayout)view.findViewById(R.id.background);
        }
    }

}

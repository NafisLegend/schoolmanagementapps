package com.techgeniusbd.schoolmanagementsystem.admin.view.common;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.Home;
import com.techgeniusbd.schoolmanagementsystem.admin.view.gurdian.home.HomeStudent;

import org.json.JSONObject;

public class SignIn extends AppCompatActivity {

    Button login;
    EditText username, password;
    String $_userName = "", $_passWord = "";
    public static String companyID = "", userType = "";

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        Log.e("===SIGN IN ===", "===SIGN IN ====");

        Log.e("==> ADMIN STATUS ",""+prefs.getInt("admin",0));




        login = (Button) findViewById(R.id.login);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                $_userName = username.getText().toString().trim();
                $_passWord = password.getText().toString().trim();


                Log.e("===ID ===", $_userName);
                Log.e("===PASS ===", $_passWord);

                new LoginDataParser().execute();


            }
        });
    }


    private class LoginDataParser extends AsyncTask<String, Void, Void> {

        String JSONString;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            Log.e("===PRE ===", "===PRE ====");
        }

        @Override
        protected Void doInBackground(String... strings) {

            try {


                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_name", $_userName);
                jsonObject.put("password", $_passWord);
                jsonObject.put("type", prefs.getInt("admin", 3));


                JSONString = Utility.post(stringHolder.login, "" + jsonObject);


                Log.e("===BACK ===", "===BACK ====");
                Log.e("Response FORM LOGIN", ""+jsonObject);
                Log.e("Response FORM LOGIN", JSONString);
                Log.e("======", "=======");

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if (prefs.getInt("admin", 3) == 1) {


                startActivity(new Intent(SignIn.this, HomeStudent.class));
                finish();
            } else if (prefs.getInt("admin", 3) == 2) {

                ParseDataAdmin(JSONString);
                startActivity(new Intent(SignIn.this, Home.class));
                finish();

            } else {

            }


        }
    }

    private void ParseDataAdmin(String jsonString) {

        try {

            JSONObject jsonObject_mother = new JSONObject(jsonString);


            JSONObject data = jsonObject_mother.getJSONObject("data");
            JSONObject oldValues = data.getJSONObject("oldValues");

            Log.e(" ****** OlD Value ===>", oldValues.toString());

            editor.putString("name", oldValues.optString("name"));
            editor.putString("user_id", oldValues.optString("password"));
            editor.putString("company_id", oldValues.optString("company_id"));
            editor.putBoolean("logedIn", true);

            editor.apply();


            companyID = oldValues.optString("company_id");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

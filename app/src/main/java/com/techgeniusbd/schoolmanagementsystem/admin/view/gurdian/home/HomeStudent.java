package com.techgeniusbd.schoolmanagementsystem.admin.view.gurdian.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.expenseList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.incomeList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.studentList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages.NoticeBoard;

public class HomeStudent extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_student);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_view, new HomeFragmentStudent().createInstance("1"))
                .addToBackStack(null)
                .commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action

        } else if (id == R.id.nav_student) {

            startActivity(new Intent(HomeStudent.this, studentList.class));

        } else if (id == R.id.nav_income) {

            startActivity(new Intent(HomeStudent.this, incomeList.class));
        } else if (id == R.id.nav_expense) {

            startActivity(new Intent(HomeStudent.this, expenseList.class));
        } else if (id == R.id.nav_noticeboard) {

            startActivity(new Intent(HomeStudent.this, NoticeBoard.class));
        } else if (id == R.id.nav_logout) {

        } else {


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class qualificationModel {

    String qualification_id;
    String qualification_name;
    String details;
    String ative;

    public String getQualification_id() {
        return qualification_id;
    }

    public void setQualification_id(String qualification_id) {
        this.qualification_id = qualification_id;
    }

    public String getQualification_name() {
        return qualification_name;
    }

    public void setQualification_name(String qualification_name) {
        this.qualification_name = qualification_name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAtive() {
        return ative;
    }

    public void setAtive(String ative) {
        this.ative = ative;
    }
}

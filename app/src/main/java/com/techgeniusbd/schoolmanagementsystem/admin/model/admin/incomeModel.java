package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class incomeModel {

    String income_type_id;
    String income_type_name;
    String school_id;
    String for_student;
    String active;

    public String getIncome_type_id() {
        return income_type_id;
    }

    public void setIncome_type_id(String income_type_id) {
        this.income_type_id = income_type_id;
    }

    public String getIncome_type_name() {
        return income_type_name;
    }

    public void setIncome_type_name(String income_type_name) {
        this.income_type_name = income_type_name;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getFor_student() {
        return for_student;
    }

    public void setFor_student(String for_student) {
        this.for_student = for_student;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}

package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.AttendenceAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.ItemClickSupport;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendenceSheet extends AppCompatActivity {

    RecyclerView attendence_list;
    private RecyclerView.LayoutManager mLayoutManager;
    AttendenceAdapter adapter;
    Toolbar myToolbar;
    private static ArrayList<studentModel> all_attendence_list;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence_sheet);

        setToolBar();
        Initialization();

        adapter = new AttendenceAdapter(all_attendence_list, getApplicationContext(), 3);
        attendence_list = (RecyclerView) findViewById(R.id.attendence_list);
        attendence_list.setLayoutManager(mLayoutManager);
        attendence_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        new ParseStudentList().execute("" + getIntent().getStringExtra("Studentobject"));

        Log.e("!!!!  ---->" ,"" + getIntent().getStringExtra("Studentobject"));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ItemClickSupport.addTo(attendence_list).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v, int type) {

            }
        });

        adapter.setGroupSMS(new AttendenceAdapter.AddAttendence() {
            @Override
            public void onAttendence(int position, View view, AttendenceAdapter.MyViewHolder holder, boolean isChecked) {

            }
        });

    }

    private void Initialization() {
        all_attendence_list = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);

    }

    private class ParseStudentList extends AsyncTask<String, Void, Void> {

        String JsonString = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JsonString = Utility.post(stringHolder.AttendenceList, strings[0]);
                Log.e("URL --> ", stringHolder.AttendenceList);
                Log.e("Object--->", strings[0]);
                Log.e("Student List--->", JsonString);


            } catch (Exception e) {
                e.printStackTrace();


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ParseAttendenceList(JsonString);
            adapter.notifyDataSetChanged();

        }
    }

    private void ParseAttendenceList(String jsonString) {

        try {

            JSONObject jsonObject = new JSONObject(jsonString);

            JSONArray student_info = jsonObject.getJSONArray("student");


            for (int i = 0; i < student_info.length(); i++) {

                studentModel model = new studentModel();

                model.setStatus_id(student_info.getJSONObject(i).optString("student_id"));
                model.setStudent_name(student_info.getJSONObject(i).optString("student_name"));
                model.setRoll_no(student_info.getJSONObject(i).optString("roll_no"));
                model.setShift_id(student_info.getJSONObject(i).optString("shift_id"));
                model.setGender(student_info.getJSONObject(i).optString("gender"));
                all_attendence_list.add(model);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.attendence_list));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }
}

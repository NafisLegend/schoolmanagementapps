package com.techgeniusbd.schoolmanagementsystem.admin.view.common;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;

import com.techgeniusbd.schoolmanagementsystem.R;

public class LogInChooser extends AppCompatActivity {

    private RadioGroup radioSexGroup;


    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_chooser);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        radioSexGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.student_rd) {


                    editor.putInt("admin", 1);
                    editor.apply();

                    startActivity(new Intent(LogInChooser.this, SignIn.class));
                    finish();



                } else if (checkedId == R.id.teacher_rd) {


                    editor.putInt("admin", 2);
                    editor.apply();


                    startActivity(new Intent(LogInChooser.this, SignIn.class));
                    finish();



                }
            }

        });






    }
}

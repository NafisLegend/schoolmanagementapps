package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

public class NoticeBoard extends AppCompatActivity {

    Toolbar myToolbar;
    AppCompatSpinner sp_shift, sp_class, sp_section;
    EditText date_of_notice;
    Button submit_noticce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);


        sp_shift = (AppCompatSpinner) findViewById(R.id.sp_shift);
        sp_class = (AppCompatSpinner) findViewById(R.id.sp_class);
        sp_section = (AppCompatSpinner) findViewById(R.id.sp_section);
        date_of_notice = (EditText) findViewById(R.id.attendence_Date);
        submit_noticce = (Button) findViewById(R.id.start_attendence);

        setToolBar();
        populateAttendence();
    }




    private void populateAttendence() {

        ArrayAdapter shift_adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, HomeFragment.all_shift_array);
        ArrayAdapter class_adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, HomeFragment.all_class_array);
        ArrayAdapter section_adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, HomeFragment.all_section_array);


        shift_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        section_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_shift.setAdapter(shift_adapter);
        sp_class.setAdapter(class_adapter);
        sp_section.setAdapter(section_adapter);

    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.notice_board));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }
}

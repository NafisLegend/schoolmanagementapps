package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.ClassListAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.classModel;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.ItemClickSupport;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addStudent;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages.StudentofClass;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import java.util.ArrayList;

public class studentList extends AppCompatActivity {

    RecyclerView classList;
    private RecyclerView.LayoutManager mLayoutManager;
  ClassListAdapter classListAdapter;
    ArrayList<classModel> classModelArrayList;
    Toolbar myToolbar;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list2);
        setToolBar();

        mLayoutManager = new LinearLayoutManager(this);
        classListAdapter=new ClassListAdapter(HomeFragment.all_class_list,getApplicationContext(),2);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(studentList.this, addStudent.class));
            }
        });




        classList = (RecyclerView) findViewById(R.id.list_all_class_list);
        classList.setLayoutManager(mLayoutManager);
        classList.setAdapter(classListAdapter);
        classListAdapter.notifyDataSetChanged();


        ItemClickSupport.addTo(classList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v, int type) {


                startActivity(new Intent(studentList.this, StudentofClass.class)
                        .putExtra("classID", HomeFragment.all_class_list.get(position).getClass_id())
                        .putExtra("className", HomeFragment.all_class_list.get(position).getClass_name())

                );
            }
        });


    }


    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.class_list));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }
}

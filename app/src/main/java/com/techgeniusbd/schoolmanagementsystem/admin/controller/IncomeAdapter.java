package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.incomeItemModel;

import java.util.ArrayList;


public class IncomeAdapter extends RecyclerView.Adapter<IncomeAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<incomeItemModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public IncomeAdapter(ArrayList<incomeItemModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_income, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String income_type_name = list_of_title.get(position).getIncome_type_name();
        String income_date = list_of_title.get(position).getDate();
        String income_name = list_of_title.get(position).getStudent_name();
        String income_roll = list_of_title.get(position).getRoll_no();
       // String income_class = list_of_title.get(position).getc;
        String income_amount = list_of_title.get(position).getAmount();

        holder.income_sector.setText(income_type_name);
        holder.incmoe_date.setText(income_date);
        holder.income_student.setText(income_name);
        holder.income_roll.setText("Roll : "+ income_roll);
        holder.income_amount.setText("Taka : "+income_amount+"/= ");


    }

    @Override
    public int getItemCount() {
        return list_of_title.size();

    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        IncomeAdapter.setRecyclerClick = setRecyclerClick;
    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView incmoe_date, income_student, income_roll, income_class, income_sector, income_amount;

        public MyViewHolder(View view) {
            super(view);
            incmoe_date = (TextView) view.findViewById(R.id.incmoe_date);
            income_student = (TextView) view.findViewById(R.id.income_student);
            income_roll = (TextView) view.findViewById(R.id.income_roll);
            income_class = (TextView) view.findViewById(R.id.income_class);
            income_sector = (TextView) view.findViewById(R.id.income_sector);
            income_amount = (TextView) view.findViewById(R.id.income_amount);

        }
    }

}

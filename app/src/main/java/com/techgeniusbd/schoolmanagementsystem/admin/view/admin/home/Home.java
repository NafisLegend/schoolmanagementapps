package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.expenseList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.incomeList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.studentList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem.teacherList;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages.NoticeBoard;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

Initialization();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_view, new HomeFragment().createInstance("1"))
                .addToBackStack(null)
                .commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    private void Initialization() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = prefs.edit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action

        } else if (id == R.id.nav_student) {

            startActivity(new Intent(Home.this, studentList.class));

        } else if (id == R.id.nav_teacher) {

            startActivity(new Intent(Home.this, teacherList.class));

        } else if (id == R.id.nav_income) {

            startActivity(new Intent(Home.this, incomeList.class));
        } else if (id == R.id.nav_expense) {

            startActivity(new Intent(Home.this, expenseList.class));
        } else if (id == R.id.nav_noticeboard) {

            startActivity(new Intent(Home.this, NoticeBoard.class));
        } else if (id == R.id.nav_logout) {

            editor.clear();
            editor.apply();
            finish();

        } else {


        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

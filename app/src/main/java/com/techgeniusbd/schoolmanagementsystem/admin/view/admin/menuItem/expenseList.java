package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.ExpenseAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addExpense;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

public class expenseList extends AppCompatActivity {

    RecyclerView list_all_expense_list;
    private RecyclerView.LayoutManager mLayoutManager;
    ExpenseAdapter adapter;
    Toolbar myToolbar;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_list);

        setToolBar();

        mLayoutManager = new LinearLayoutManager(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(expenseList.this, addExpense.class));
            }
        });



        adapter = new ExpenseAdapter(HomeFragment.all_expenseItemModel_list, getApplicationContext(), 3);
        list_all_expense_list = (RecyclerView) findViewById(R.id.list_all_expense_list);
        list_all_expense_list.setLayoutManager(mLayoutManager);
        list_all_expense_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.expense_list));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }
}

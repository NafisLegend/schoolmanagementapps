package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class studentModel {
    private String student_id;
    private String student_code;
    private String student_name;
    private String student_name_bn;
    private String passward;
    private String shift_id;
    private String class_id;
    private String section_id;
    private String roll_no;
    private String group_id;
    private String dob;
    private String admission_date;
    private String status_id;
    private String mobile;
    private String company_id;
    private String father_name;
    private String mother_name;
    private String sms_mobile_no;
    private String email_notify;
    private String pre_address;
    private String perm_address;
    private String parent_profession_id;
    private String profession_details;
    private String active;
    private String photo;
    private String batch_id;
    private String mother_sms;
    private String program_id;
    private String sms_deliverable;
    private String blood_group;
    private String religion;
    private String gender;
    private String fourth_subject_id;
    private String excluded_subject_id;
    private String guardian_name;
    private String guardian_photo;
    private String father_photo;
    private String mother_photo;

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_code() {
        return student_code;
    }

    public void setStudent_code(String student_code) {
        this.student_code = student_code;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStudent_name_bn() {
        return student_name_bn;
    }

    public void setStudent_name_bn(String student_name_bn) {
        this.student_name_bn = student_name_bn;
    }

    public String getPassward() {
        return passward;
    }

    public void setPassward(String passward) {
        this.passward = passward;
    }

    public String getShift_id() {
        return shift_id;
    }

    public void setShift_id(String shift_id) {
        this.shift_id = shift_id;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAdmission_date() {
        return admission_date;
    }

    public void setAdmission_date(String admission_date) {
        this.admission_date = admission_date;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public String getSms_mobile_no() {
        return sms_mobile_no;
    }

    public void setSms_mobile_no(String sms_mobile_no) {
        this.sms_mobile_no = sms_mobile_no;
    }

    public String getEmail_notify() {
        return email_notify;
    }

    public void setEmail_notify(String email_notify) {
        this.email_notify = email_notify;
    }

    public String getPre_address() {
        return pre_address;
    }

    public void setPre_address(String pre_address) {
        this.pre_address = pre_address;
    }

    public String getPerm_address() {
        return perm_address;
    }

    public void setPerm_address(String perm_address) {
        this.perm_address = perm_address;
    }

    public String getParent_profession_id() {
        return parent_profession_id;
    }

    public void setParent_profession_id(String parent_profession_id) {
        this.parent_profession_id = parent_profession_id;
    }

    public String getProfession_details() {
        return profession_details;
    }

    public void setProfession_details(String profession_details) {
        this.profession_details = profession_details;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBatch_id() {
        return batch_id;
    }

    public void setBatch_id(String batch_id) {
        this.batch_id = batch_id;
    }

    public String getMother_sms() {
        return mother_sms;
    }

    public void setMother_sms(String mother_sms) {
        this.mother_sms = mother_sms;
    }

    public String getProgram_id() {
        return program_id;
    }

    public void setProgram_id(String program_id) {
        this.program_id = program_id;
    }

    public String getSms_deliverable() {
        return sms_deliverable;
    }

    public void setSms_deliverable(String sms_deliverable) {
        this.sms_deliverable = sms_deliverable;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFourth_subject_id() {
        return fourth_subject_id;
    }

    public void setFourth_subject_id(String fourth_subject_id) {
        this.fourth_subject_id = fourth_subject_id;
    }

    public String getExcluded_subject_id() {
        return excluded_subject_id;
    }

    public void setExcluded_subject_id(String excluded_subject_id) {
        this.excluded_subject_id = excluded_subject_id;
    }

    public String getGuardian_name() {
        return guardian_name;
    }

    public void setGuardian_name(String guardian_name) {
        this.guardian_name = guardian_name;
    }

    public String getGuardian_photo() {
        return guardian_photo;
    }

    public void setGuardian_photo(String guardian_photo) {
        this.guardian_photo = guardian_photo;
    }

    public String getFather_photo() {
        return father_photo;
    }

    public void setFather_photo(String father_photo) {
        this.father_photo = father_photo;
    }

    public String getMother_photo() {
        return mother_photo;
    }

    public void setMother_photo(String mother_photo) {
        this.mother_photo = mother_photo;
    }
}

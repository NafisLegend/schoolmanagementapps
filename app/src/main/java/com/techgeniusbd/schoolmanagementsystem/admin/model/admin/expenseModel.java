package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class expenseModel {

    String expense_type_id;
    String expense_type_name;
    String school_id;
    String active;

    public String getExpense_type_id() {
        return expense_type_id;
    }

    public void setExpense_type_id(String expense_type_id) {
        this.expense_type_id = expense_type_id;
    }

    public String getExpense_type_name() {
        return expense_type_name;
    }

    public void setExpense_type_name(String expense_type_name) {
        this.expense_type_name = expense_type_name;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}

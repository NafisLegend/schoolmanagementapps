package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.StudentAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StudentofClass extends AppCompatActivity {

    RecyclerView studnetList;
    private RecyclerView.LayoutManager mLayoutManager;
    StudentAdapter adapter;
    ArrayList<studentModel> all_student_list;
    Toolbar myToolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        setToolBar();


        all_student_list = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        studnetList = (RecyclerView) findViewById(R.id.studnet_list);
        studnetList.setLayoutManager(mLayoutManager);
        adapter = new StudentAdapter(all_student_list, this, 1);
        studnetList.setAdapter(adapter);

        filterClassData(HomeFragment.all_configuration_data, getIntent().getStringExtra("classID"));

        adapter.notifyDataSetChanged();

    }

    private void filterClassData(String allConfigurationData, String classID) {

        try {
            JSONObject jsonObject = new JSONObject(allConfigurationData);
            JSONArray all_student = jsonObject.getJSONArray("student");

            all_student_list.clear();

            for (int i = 0; i < all_student.length(); i++) {

                studentModel model = new studentModel();

                if(all_student.getJSONObject(i).optString("class_id").contentEquals(classID)){

                    model.setStudent_name(all_student.getJSONObject(i).optString("student_name"));
                    model.setRoll_no(all_student.getJSONObject(i).optString("roll_no"));
                    model.setShift_id(all_student.getJSONObject(i).optString("shift_id"));
                    model.setGender(all_student.getJSONObject(i).optString("gender"));

                    all_student_list.add(model);

                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("className")+" student");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }
}

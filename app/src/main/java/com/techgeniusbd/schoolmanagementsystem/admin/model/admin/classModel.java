package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class classModel {


    public String class_id;
    public String class_name;
    public String active;
    public String company_id;
    public String sl;
    public String numeric_value;
    public String numberofStudnets;


    public String getNumberofStudnets() {
        return numberofStudnets;
    }

    public void setNumberofStudnets(String numberofStudnets) {
        this.numberofStudnets = numberofStudnets;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getNumeric_value() {
        return numeric_value;
    }

    public void setNumeric_value(String numeric_value) {
        this.numeric_value = numeric_value;
    }
}

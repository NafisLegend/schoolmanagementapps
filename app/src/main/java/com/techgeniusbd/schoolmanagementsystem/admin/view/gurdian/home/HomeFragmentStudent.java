package com.techgeniusbd.schoolmanagementsystem.admin.view.gurdian.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techgeniusbd.schoolmanagementsystem.R;


public class HomeFragmentStudent extends Fragment {

    View view;

    public HomeFragmentStudent() {

    }

    public static HomeFragmentStudent createInstance(String itemsCount) {
        HomeFragmentStudent homeFargment = new HomeFragmentStudent();
        Bundle bundle = new Bundle();
        bundle.putString("Id", itemsCount);
        homeFargment.setArguments(bundle);
        return homeFargment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home_student, container, false);


        return view;
    }


    @Override
    public void onPause() {
        super.onPause();

    }
}




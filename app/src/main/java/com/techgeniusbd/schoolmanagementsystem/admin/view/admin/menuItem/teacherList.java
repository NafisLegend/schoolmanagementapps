package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.menuItem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.TeacherAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.horizontalAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.subjectModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.teacherModel;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addTeacher;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import java.util.ArrayList;

public class teacherList extends AppCompatActivity {

    FloatingActionButton fab;
    Toolbar myToolbar;
    RecyclerView.LayoutManager layoutManager,mLayoutManagerHorizontal1;
    horizontalAdapter subjectadapter;
    TeacherAdapter teacherAdapter;
    ArrayList<teacherModel> filter_teacher_list;

    RecyclerView teacher_list, subject_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_list);

        setToolBar();
        Initialization();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(teacherList.this, addTeacher.class));
            }
        });


        subjectadapter.setOnItemClickListener(new horizontalAdapter.RecyclerClick() {
            @Override
            public void OnItemClick(int position, View view) {

                filterTeacherList(HomeFragment.all_subject_list.get(position).getSubject_id());

            }
        });



    }

    private void Initialization() {

        filter_teacher_list=new ArrayList<>();
        filter_teacher_list.clear();

        filter_teacher_list.addAll(HomeFragment.all_teacher_list);

        teacher_list = (RecyclerView) findViewById(R.id.list_all_teacher);
        subject_list = (RecyclerView) findViewById(R.id.subject_list);

        layoutManager = new LinearLayoutManager(this);
        mLayoutManagerHorizontal1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        teacherAdapter = new TeacherAdapter(filter_teacher_list, getApplicationContext(), 1);
        subjectadapter=new horizontalAdapter(HomeFragment.all_subject_list,getApplicationContext(),1);

        teacher_list.setLayoutManager(layoutManager);
        teacher_list.setAdapter(teacherAdapter);

        subject_list.setLayoutManager(mLayoutManagerHorizontal1);
        subject_list.setAdapter(subjectadapter);

        subjectadapter.notifyDataSetChanged();
        teacherAdapter.notifyDataSetChanged();
    }

    private void filterTeacherList(String subject_id) {

        filter_teacher_list.clear();
        for (teacherModel model:HomeFragment.all_teacher_list){

           if( model.getSubject_id().contentEquals(subject_id)){
               filter_teacher_list.add(model);

           }
        }
        teacherAdapter.notifyDataSetChanged();
    }

    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.teacher_list));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
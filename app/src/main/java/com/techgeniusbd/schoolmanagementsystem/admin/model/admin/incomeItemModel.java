package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class incomeItemModel {

    String income_type_id;
    String amount;
    String month;
    String year;
    String date;
    String income_type_name;
    String student_id;
    String student_code;
    String student_name;
    String roll_no;


    public String getRoll_no() {
        return roll_no;
    }

    public void setRoll_no(String roll_no) {
        this.roll_no = roll_no;
    }


    public String getIncome_type_id() {
        return income_type_id;
    }

    public void setIncome_type_id(String income_type_id) {
        this.income_type_id = income_type_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIncome_type_name() {
        return income_type_name;
    }

    public void setIncome_type_name(String income_type_name) {
        this.income_type_name = income_type_name;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_code() {
        return student_code;
    }

    public void setStudent_code(String student_code) {
        this.student_code = student_code;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }
}

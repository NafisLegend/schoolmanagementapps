package com.techgeniusbd.schoolmanagementsystem.admin.model.admin;

public class expenseItemModel {

    String expense_id;
    String expense_type_id;
    String date;
    String amount;
    String remarks;
    String expense_type_name;
    String active;


    public String getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(String expense_id) {
        this.expense_id = expense_id;
    }

    public String getExpense_type_id() {
        return expense_type_id;
    }

    public void setExpense_type_id(String expense_type_id) {
        this.expense_type_id = expense_type_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getExpense_type_name() {
        return expense_type_name;
    }

    public void setExpense_type_name(String expense_type_name) {
        this.expense_type_name = expense_type_name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}

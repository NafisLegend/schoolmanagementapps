package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.teacherModel;

import java.util.ArrayList;


public class TeacherAdapter extends RecyclerView.Adapter<TeacherAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<teacherModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public TeacherAdapter(ArrayList<teacherModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_teacher, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String title = list_of_title.get(position).getName();
        String roll = list_of_title.get(position).getQualification_details();
        String shift = list_of_title.get(position).getMobile();
        String gender = list_of_title.get(position).getGender();

        holder.name.setText(title);
        holder.education.setText(""+roll);
        holder.mobile.setText("Mobile : "+shift);



    }

    @Override
    public int getItemCount() {
        return list_of_title.size();
    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        TeacherAdapter.setRecyclerClick = setRecyclerClick;
    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, education, mobile;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name_of_teacher);
            education = (TextView) view.findViewById(R.id.educational_teacher);
            mobile = (TextView) view.findViewById(R.id.mobile_number_teacher);


        }
    }

}

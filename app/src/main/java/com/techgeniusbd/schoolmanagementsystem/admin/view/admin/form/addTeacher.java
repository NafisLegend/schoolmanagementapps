package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home.HomeFragment;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;

public class addTeacher extends AppCompatActivity {

    Toolbar myToolbar;
    AppCompatSpinner sp_shift, sp_subject, sp_school, sp_qualification;
    EditText teacher_Name, teacher_qualification, teacher_joiningDate,
            teacher_MotherName, teacher_fatherName, teacher_mobile,
            teacher_landPhone, teacher_email, teacher_presentAddress,
            teacher_permanentAddress;
    FloatingActionButton add_teacher;
    final Calendar myCalendar = Calendar.getInstance();
    String $_teachersName, $_shift, $_qualifications, $_qualificationDetails,
            $_joiningDate, $_teachingSubject, $_schoolName, $_fatherName,
            $_MotherName, $_mobile, $_landPhone, $_email, $_presentAddress,
            $_permanentAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_teacher);

        setToolBar();
        Initialization();

        add_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetValues();
                SendDataToserver();
            }
        });
    }

    private void GetValues() {

        $_teachersName = teacher_Name.getText().toString();
        $_shift = HomeFragment.all_shift_list.get(sp_shift.getSelectedItemPosition()).getShift_id();
        $_qualifications = HomeFragment.all_qualification_list.get(sp_qualification.getSelectedItemPosition()).getQualification_id();

        $_qualificationDetails = teacher_qualification.getText().toString();
        $_joiningDate = teacher_joiningDate.getText().toString();
        $_teachingSubject = HomeFragment.all_subject_list.get(sp_subject.getSelectedItemPosition()).getSubject_id();
        $_schoolName = HomeFragment.all_school_list.get(sp_school.getSelectedItemPosition()).getCompany_id();

        $_fatherName = teacher_fatherName.getText().toString();
        $_MotherName = teacher_MotherName.getText().toString();
        $_mobile = teacher_mobile.getText().toString();
        $_landPhone = teacher_landPhone.getText().toString();
        $_email = teacher_email.getText().toString();
        $_presentAddress = teacher_presentAddress.getText().toString();
        $_permanentAddress = teacher_permanentAddress.getText().toString();

    }

    private void SendDataToserver() {

        new SendDataServer().execute();
    }

    private class SendDataServer extends AsyncTask<String, Void, Void> {

        String JsonString;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", $_teachersName);
                jsonObject.put("shift", $_shift);
                jsonObject.put("qualification", "");
                jsonObject.put("qualification", $_qualificationDetails);
                jsonObject.put("joinDate", $_joiningDate);
                jsonObject.put("subject", $_teachingSubject);
                jsonObject.put("school", $_schoolName);
                jsonObject.put("fatherName", $_fatherName);
                jsonObject.put("motherName", $_MotherName);
                jsonObject.put("mobile", $_mobile);
                jsonObject.put("landPhone", $_landPhone);
                jsonObject.put("email", $_email);
                jsonObject.put("presentAddress", $_presentAddress);
                jsonObject.put("permanentAddress", $_permanentAddress);

                Log.e("JsonObject", "" + jsonObject);
                JsonString = Utility.post(stringHolder.AddTeacher, "" + jsonObject);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void Initialization() {

        sp_shift = (AppCompatSpinner) findViewById(R.id.sp_shift);
        sp_subject = (AppCompatSpinner) findViewById(R.id.sp_subject);
        sp_school = (AppCompatSpinner) findViewById(R.id.sp_school);
        sp_qualification = (AppCompatSpinner) findViewById(R.id.sp_qualification);
        add_teacher = (FloatingActionButton) findViewById(R.id.fab_add_teacher);

        ArrayAdapter shift_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_shift_array);
        ArrayAdapter subject_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_subject_array);
        ArrayAdapter school_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_school_array);
        ArrayAdapter qualification_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, HomeFragment.all_qualification_array);

        shift_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subject_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        school_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualification_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_shift.setAdapter(shift_adapter);
        sp_subject.setAdapter(subject_adapter);
        sp_school.setAdapter(school_adapter);
        sp_qualification.setAdapter(qualification_adapter);

        teacher_Name = (EditText) findViewById(R.id.addTeacher_Name);
        teacher_qualification = (EditText) findViewById(R.id.addTeacher_qualification);
        teacher_joiningDate = (EditText) findViewById(R.id.addTeacher_joiningDate);
        teacher_MotherName = (EditText) findViewById(R.id.addTeacher_MotherName);
        teacher_fatherName = (EditText) findViewById(R.id.addTeacher_fatherName);
        teacher_mobile = (EditText) findViewById(R.id.addTeacher_mobile);
        teacher_landPhone = (EditText) findViewById(R.id.addTeacher_landPhone);
        teacher_email = (EditText) findViewById(R.id.addTeacher_email);
        teacher_presentAddress = (EditText) findViewById(R.id.addTeacher_presentAddress);
        teacher_permanentAddress = (EditText) findViewById(R.id.addTeacher_permanentAddress);

        teacher_joiningDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(addTeacher.this, DateListner,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });
        $_shift = HomeFragment.all_shift_list.get(0).getShift_id();
        $_qualifications = HomeFragment.all_qualification_list.get(0).getQualification_id();
        $_teachingSubject = HomeFragment.all_subject_list.get(0).getSubject_id();
        $_schoolName = HomeFragment.all_school_list.get(0).getCompany_id();
    }


    private void setToolBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(this.getString(R.string.add_teacher));

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    DatePickerDialog.OnDateSetListener DateListner = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateBirthDate();

        }
    };

    private void updateBirthDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(myCalendar.getTime());
        teacher_joiningDate.setText(date);

    }
}

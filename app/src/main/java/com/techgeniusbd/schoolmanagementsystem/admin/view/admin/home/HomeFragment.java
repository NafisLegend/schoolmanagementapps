package com.techgeniusbd.schoolmanagementsystem.admin.view.admin.home;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.api.stringHolder;
import com.techgeniusbd.schoolmanagementsystem.admin.controller.ClassAdapter;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.classModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.expenseItemModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.expenseModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.groupModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.incomeItemModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.incomeModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.professionModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.qualificationModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.schoolModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.sectionModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.shiftModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.subjectModel;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.teacherModel;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.ItemClickSupport;
import com.techgeniusbd.schoolmanagementsystem.admin.utility.Utility;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages.AttendenceSheet;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addExpense;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addIncome;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addStudent;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.form.addTeacher;
import com.techgeniusbd.schoolmanagementsystem.admin.view.admin.pages.StudentofClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;


public class HomeFragment extends Fragment {
    TextView schoolName, phoneNumber, mobileNumber, eMail;
    public static String company_name = "", email = "", mobile = "", phone = "";
    public static ArrayList<classModel> all_class_list;
    public static ArrayList<groupModel> all_group_list;
    public static ArrayList<sectionModel> all_section_list;
    public static ArrayList<shiftModel> all_shift_list;
    public static ArrayList<teacherModel> all_teacher_list;
    public static ArrayList<subjectModel> all_subject_list;
    public static ArrayList<incomeModel> all_income_list;
    public static ArrayList<expenseModel> all_expense_list;
    public static ArrayList<schoolModel> all_school_list;
    public static ArrayList<studentModel> all_student_list;

    public static ArrayList<professionModel> all_profession_list;
    public static ArrayList<qualificationModel> all_qualification_list;
    public static ArrayList<expenseItemModel> all_expenseItemModel_list;
    public static ArrayList<incomeItemModel> all_incomeItem_list;
    AppCompatSpinner sp_shift, sp_class, sp_section;
    EditText et_attendence_date;

    View view;
    Button start_attendence;
    public static ArrayList<String> all_class_array, all_group_array, all_section_array, all_shift_array,
            all_income_array, all_expense_array, all_subject_array, all_school_array, all_student_array, all_profession_array, all_qualification_array;
    FloatingActionButton student_addmission, settings, teacher, add_expense, add_income;
    RecyclerView classList;
    RecyclerView.LayoutManager mLayoutManager;
    ClassAdapter adapter;
    public static String all_configuration_data = "";
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    final Calendar myCalendar = Calendar.getInstance();

    public HomeFragment() {

    }

    public static HomeFragment createInstance(String itemsCount) {
        HomeFragment homeFargment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Id", itemsCount);
        homeFargment.setArguments(bundle);
        return homeFargment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home_, container, false);
        ViewGeneralSection(view);

        new ParseAllConfigurationData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new ParseExpenseIncomeData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        ItemClickSupport.addTo(classList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v, int type) {


                startActivity(new Intent(getActivity(), StudentofClass.class)
                        .putExtra("classID", all_class_list.get(position).getClass_id())
                        .putExtra("className", all_class_list.get(position).getClass_name())
                );
            }
        });

        student_addmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), addStudent.class));
            }
        });

        teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), addTeacher.class));
            }
        });

        add_income.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), addIncome.class));
            }
        });

        add_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), addExpense.class));

            }
        });

        return view;
    }

    private void ViewGeneralSection(View view) {

        schoolName = (TextView) view.findViewById(R.id.schoolName);
        phoneNumber = (TextView) view.findViewById(R.id.phonenumber);
        mobileNumber = (TextView) view.findViewById(R.id.mobile);
        eMail = (TextView) view.findViewById(R.id.email);

        all_class_list = new ArrayList<>();
        all_group_list = new ArrayList<>();
        all_section_list = new ArrayList<>();
        all_shift_list = new ArrayList<>();
        all_subject_list = new ArrayList<>();
        all_teacher_list = new ArrayList<>();
        all_income_list = new ArrayList<>();
        all_expense_list = new ArrayList<>();
        all_school_list = new ArrayList<>();
        all_student_list = new ArrayList<>();
        all_profession_list = new ArrayList<>();
        all_qualification_list = new ArrayList<>();
        all_expenseItemModel_list = new ArrayList<>();
        all_incomeItem_list = new ArrayList<>();


        all_class_array = new ArrayList<>();
        all_group_array = new ArrayList<>();
        all_section_array = new ArrayList<>();
        all_shift_array = new ArrayList<>();
        all_subject_array = new ArrayList<>();
        all_income_array = new ArrayList<>();
        all_expense_array = new ArrayList<>();
        all_school_array = new ArrayList<>();
        all_student_array = new ArrayList<>();
        all_profession_array = new ArrayList<>();
        all_qualification_array = new ArrayList<>();

        classList = (RecyclerView) view.findViewById(R.id.classList);
        adapter = new ClassAdapter(all_class_list, getContext(), 1);
        classList.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        classList.setItemAnimator(new DefaultItemAnimator());
        classList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        student_addmission = (FloatingActionButton) view.findViewById(R.id.fab_add_student);
        add_income = (FloatingActionButton) view.findViewById(R.id.fab_add_income);
        add_expense = (FloatingActionButton) view.findViewById(R.id.fab_add_expense);
        teacher = (FloatingActionButton) view.findViewById(R.id.fab_add_teacher);

        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = prefs.edit();

        sp_shift = (AppCompatSpinner) view.findViewById(R.id.sp_shift);
        sp_class = (AppCompatSpinner) view.findViewById(R.id.sp_class);
        sp_section = (AppCompatSpinner) view.findViewById(R.id.sp_section);
        et_attendence_date = (EditText) view.findViewById(R.id.attendence_Date);
        start_attendence = (Button) view.findViewById(R.id.start_attendence);

        start_attendence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getValuesFromSpinner();
            }
        });


        et_attendence_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), dateSetListener,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(MONTH),
                        myCalendar.get(DAY_OF_MONTH))
                        .show();
            }
        });

    }

    private void getValuesFromSpinner() {


        int shift_pos = ((Spinner) view.findViewById(R.id.sp_shift)).getSelectedItemPosition();
        int class_pos = ((Spinner) view.findViewById(R.id.sp_class)).getSelectedItemPosition();
        int section_pos = ((Spinner) view.findViewById(R.id.sp_section)).getSelectedItemPosition();

        String shiftID = all_shift_list.get(shift_pos).getShift_id();
        String classID = all_class_list.get(class_pos).getClass_id();
        String sectionID = all_section_list.get(section_pos).getSection_id();

        Log.e("SHIFT", "" + shiftID);
        Log.e("CLASS", "" + classID);
        Log.e("SECTION", "" + sectionID);

        try {

            JSONObject jsonObject = new JSONObject();


            jsonObject.put("company_id", prefs.getString("company_id", ""));
            jsonObject.put("section_id", sectionID);
            jsonObject.put("class_id", classID);
            jsonObject.put("shift_id", shiftID);


            startActivity(new Intent(getActivity(),AttendenceSheet.class)
                    .putExtra("Studentobject",""+jsonObject));

        } catch (Exception e) {
            e.printStackTrace();

        }


    }



    private class ParseAllConfigurationData extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            String JsonString;

            try {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("company_id", prefs.getString("company_id", ""));

                all_configuration_data = Utility.post(stringHolder.allConfigurationData, "" + jsonObject);


            } catch (Exception e) {
                e.printStackTrace();


                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ParseConfigurationData(all_configuration_data);

        }
    }


    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateDate();

        }
    };


    class ParseExpenseIncomeData extends AsyncTask<String, Void, Void> {

        String JsonString = "";

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {


            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("company_id", prefs.getString("company_id", ""));

                JsonString = Utility.post(stringHolder.ExpenseIncome, "" + jsonObject);

                Log.e("==> URL", stringHolder.ExpenseIncome);
                Log.e("==> OBJECT", "" + jsonObject);
                Log.e("==> URL", JsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ParseDataExpensenIncome(JsonString);
        }
    }

    private void ParseDataExpensenIncome(String jsonString) {

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray income_info = jsonObject.getJSONArray("income");
            JSONArray expanse_info = jsonObject.getJSONArray("expense");

            for (int i = 0; i < income_info.length(); i++) {
                incomeItemModel model = new incomeItemModel();
                model.setAmount(income_info.getJSONObject(i).optString("amount"));
                model.setDate(income_info.getJSONObject(i).optString("date"));
                model.setIncome_type_name(income_info.getJSONObject(i).optString("income_type_name"));
                model.setMonth(income_info.getJSONObject(i).optString("month"));
                model.setStudent_code(income_info.getJSONObject(i).optString("student_code"));
                model.setStudent_name(income_info.getJSONObject(i).optString("student_name"));
                model.setIncome_type_id(income_info.getJSONObject(i).optString("income_type_id"));
                model.setYear(income_info.getJSONObject(i).optString("year"));
                model.setRoll_no(income_info.getJSONObject(i).optString("roll_no"));
                all_incomeItem_list.add(model);

            }
            Log.e("INCOME VALUE  00 ","**** > "+HomeFragment.all_incomeItem_list.size());


            for (int i = 0; i < expanse_info.length(); i++) {

                expenseItemModel model = new expenseItemModel();
                model.setActive(expanse_info.getJSONObject(i).optString("active"));
                model.setAmount(expanse_info.getJSONObject(i).optString("amount"));
                model.setDate(expanse_info.getJSONObject(i).optString("date"));
                model.setExpense_id(expanse_info.getJSONObject(i).optString("expense_id"));
                model.setExpense_type_id(expanse_info.getJSONObject(i).optString("expense_type_id"));
                model.setExpense_type_name(expanse_info.getJSONObject(i).optString("expense_type_name"));
                model.setRemarks(expanse_info.getJSONObject(i).optString("remarks"));

                all_expenseItemModel_list.add(model);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String date = formatter.format(myCalendar.getTime());
        et_attendence_date.setText(date);

    }


    private void ParseConfigurationData(String all_configuration_data) {


        try {

            JSONObject jsonObject = new JSONObject(all_configuration_data);
            JSONArray school_info = jsonObject.getJSONArray("school_info");
            JSONArray class_info = jsonObject.getJSONArray("class");
            JSONArray shift_info = jsonObject.getJSONArray("shift");
            JSONArray student_info = jsonObject.getJSONArray("student");
            JSONArray section_info = jsonObject.getJSONArray("section");
            JSONArray group_info = jsonObject.getJSONArray("group");
            JSONArray subject_info = jsonObject.getJSONArray("subject");
            JSONArray teacher_info = jsonObject.getJSONArray("teacher");
            JSONArray income_info = jsonObject.getJSONArray("income");
            JSONArray expense_info = jsonObject.getJSONArray("expense");
            JSONArray profesion_info = jsonObject.getJSONArray("profesion");
            JSONArray qualification_info = jsonObject.getJSONArray("qualification");


            company_name = school_info.getJSONObject(0).optString("company_name");
            email = school_info.getJSONObject(0).optString("email");
            mobile = school_info.getJSONObject(0).optString("mobile");
            phone = school_info.getJSONObject(0).optString("phone");


            all_class_list.clear();
            all_group_list.clear();
            all_section_list.clear();
            all_shift_list.clear();
            all_subject_list.clear();
            all_teacher_list.clear();
            all_income_list.clear();
            all_expense_list.clear();
            all_school_list.clear();
            all_incomeItem_list.clear();

            all_class_array.clear();
            all_group_array.clear();
            all_section_array.clear();
            all_shift_array.clear();
            all_subject_array.clear();
            all_income_array.clear();
            all_expense_array.clear();
            all_school_array.clear();


            {
                schoolModel model = new schoolModel();
                model.setCompany_code(school_info.getJSONObject(0).optString("company_code"));
                model.setCompany_id(school_info.getJSONObject(0).optString("company_id"));
                model.setCompany_name(school_info.getJSONObject(0).optString("company_name"));

                all_school_array.add(school_info.getJSONObject(0).optString("company_name"));


                all_school_list.add(model);
            }


            for (int i = 0; i < profesion_info.length(); i++) {

                professionModel model = new professionModel();

                model.setProfession_id(profesion_info.getJSONObject(i).optString("profession_id"));
                model.setProfession_name(profesion_info.getJSONObject(i).optString("profession_name"));

                all_profession_list.add(model);
                all_profession_array.add(profesion_info.getJSONObject(i).optString("profession_name"));


                Log.e("Profession Array", "==> " + all_profession_array.size());
            }

            for (int i = 0; i < qualification_info.length(); i++) {
                qualificationModel model = new qualificationModel();
                model.setAtive(qualification_info.getJSONObject(i).optString("ative"));
                model.setDetails(qualification_info.getJSONObject(i).optString("details"));
                model.setQualification_id(qualification_info.getJSONObject(i).optString("qualification_id"));
                model.setQualification_name(qualification_info.getJSONObject(i).optString("qualification_name"));

                all_qualification_list.add(model);
                all_qualification_array.add(qualification_info.getJSONObject(i).optString("qualification_name"));


            }


            for (int i = 0; i < student_info.length(); i++) {

                studentModel model = new studentModel();


                model.setStatus_id(student_info.getJSONObject(i).optString("student_id"));
                model.setStudent_name(student_info.getJSONObject(i).optString("student_name"));
                model.setRoll_no(student_info.getJSONObject(i).optString("roll_no"));
                model.setShift_id(student_info.getJSONObject(i).optString("shift_id"));
                model.setGender(student_info.getJSONObject(i).optString("gender"));

                all_student_array.add(" ( " + student_info.getJSONObject(i).optString("roll_no") + " ) "
                        + student_info.getJSONObject(i).optString("student_name"));
                all_student_list.add(model);

            }


            for (int i = 0; i < teacher_info.length(); i++) {


                teacherModel model = new teacherModel();

                model.setActive(teacher_info.getJSONObject(i).optString("active"));
                model.setCompany_id(teacher_info.getJSONObject(i).optString("company_id"));
                model.setDesignation_id(teacher_info.getJSONObject(i).optString("designation_id"));
                model.setDob(teacher_info.getJSONObject(i).optString("dob"));
                model.setEmail(teacher_info.getJSONObject(i).optString("email"));
                model.setEmployee_code(teacher_info.getJSONObject(i).optString("employee_code"));
                model.setEmployee_id(teacher_info.getJSONObject(i).optString("employee_id"));
                model.setFathers_name(teacher_info.getJSONObject(i).optString("fathers_name"));
                model.setGender(teacher_info.getJSONObject(i).optString("gender"));
                model.setHr_type_id(teacher_info.getJSONObject(i).optString("hr_type_id"));
                model.setJoining_date(teacher_info.getJSONObject(i).optString("joining_date"));
                model.setMothers_name(teacher_info.getJSONObject(i).optString("mothers_name"));
                model.setMobile(teacher_info.getJSONObject(i).optString("mobile"));
                model.setName(teacher_info.getJSONObject(i).optString("name"));
                model.setNid(teacher_info.getJSONObject(i).optString("nid"));
                model.setPerm_address(teacher_info.getJSONObject(i).optString("perm_address"));
                model.setPhone(teacher_info.getJSONObject(i).optString("phone"));
                model.setPhoto(teacher_info.getJSONObject(i).optString("photo"));
                model.setPre_address(teacher_info.getJSONObject(i).optString("pre_address"));
                model.setQualification_details(teacher_info.getJSONObject(i).optString("qualification_details"));
                model.setQualification_id(teacher_info.getJSONObject(i).optString("qualification_id"));
                model.setReligion(teacher_info.getJSONObject(i).optString("religion"));
                model.setSchool_id(teacher_info.getJSONObject(i).optString("school_id"));
                model.setShift_id(teacher_info.getJSONObject(i).optString("shift_id"));
                model.setSubject_id(teacher_info.getJSONObject(i).optString("subject_id"));
                model.setUser_id(teacher_info.getJSONObject(i).optString("user_id"));
                all_teacher_list.add(model);

            }

            for (int i = 0; i < income_info.length(); i++) {
                incomeModel model = new incomeModel();
                model.setActive(income_info.getJSONObject(i).optString("active"));
                model.setFor_student(income_info.getJSONObject(i).optString("for_student"));
                model.setIncome_type_id(income_info.getJSONObject(i).optString("income_type_id"));
                model.setIncome_type_name(income_info.getJSONObject(i).optString("income_type_name"));
                model.setSchool_id(income_info.getJSONObject(i).optString("school_id"));

                all_income_array.add(income_info.getJSONObject(i).optString("income_type_name"));
                all_income_list.add(model);
            }

            for (int i = 0; i < expense_info.length(); i++) {

                expenseModel model = new expenseModel();

                model.setActive(expense_info.getJSONObject(i).optString("active"));
                model.setExpense_type_id(expense_info.getJSONObject(i).optString("expense_type_id"));
                model.setExpense_type_name(expense_info.getJSONObject(i).optString("expense_type_name"));
                model.setSchool_id(expense_info.getJSONObject(i).optString("school_id"));

                all_expense_array.add(expense_info.getJSONObject(i).optString("expense_type_name"));
                all_expense_list.add(model);


                Log.e("====> EXPENSE ", "" + all_expense_array.size());

            }


            for (int i = 0; i < class_info.length(); i++) {
                classModel model = new classModel();
                model.setActive(class_info.getJSONObject(i).optString("active"));
                model.setClass_id(class_info.getJSONObject(i).optString("class_id"));
                model.setClass_name(class_info.getJSONObject(i).optString("class_name"));
                model.setCompany_id(class_info.getJSONObject(i).optString("company_id"));

                all_class_array.add(class_info.getJSONObject(i).optString("class_name"));
                all_class_list.add(model);

            }

            for (int i = 0; i < shift_info.length(); i++) {

                shiftModel model = new shiftModel();


                model.setActive(shift_info.getJSONObject(i).optString("active"));
                model.setCompany_id(shift_info.getJSONObject(i).optString("company_id"));
                model.setShift_id(shift_info.getJSONObject(i).optString("shift_id"));
                model.setShift_name(shift_info.getJSONObject(i).optString("shift_name"));


                all_shift_array.add(shift_info.getJSONObject(i).optString("shift_name"));
                all_shift_list.add(model);

            }


            for (int i = 0; i < section_info.length(); i++) {

                sectionModel model = new sectionModel();

                model.setActive(section_info.getJSONObject(i).optString("active"));
                model.setCompany_id(section_info.getJSONObject(i).optString("company_id"));
                model.setSection_id(section_info.getJSONObject(i).optString("section_id"));
                model.setSection_name(section_info.getJSONObject(i).optString("section_name"));


                all_section_array.add(section_info.getJSONObject(i).optString("section_name"));
                all_section_list.add(model);

            }
            for (int i = 0; i < group_info.length(); i++) {

                groupModel model = new groupModel();
                model.setActive(group_info.getJSONObject(i).optString("active"));
                model.setCompany_id(group_info.getJSONObject(i).optString("company_id"));
                model.setGroup_id(group_info.getJSONObject(i).optString("group_id"));
                model.setGroup_name(group_info.getJSONObject(i).optString("group_name"));


                all_group_array.add(group_info.getJSONObject(i).optString("group_name"));
                all_group_list.add(model);


            }

            for (int i = 0; i < subject_info.length(); i++) {


                subjectModel model = new subjectModel();
                model.setActive(subject_info.getJSONObject(i).optString("active"));
                model.setClassS(subject_info.getJSONObject(i).optString("class"));
                model.setCompany_id(subject_info.getJSONObject(i).optString("company_id"));
                model.setGroup_id(subject_info.getJSONObject(i).optString("group_id"));
                model.setOptional_status(subject_info.getJSONObject(i).optString("optional_status"));
                model.setPass_marks(subject_info.getJSONObject(i).optString("pass_marks"));
                model.setPass_marks_obj(subject_info.getJSONObject(i).optString("pass_marks_obj"));
                model.setPass_marks_pra(subject_info.getJSONObject(i).optString("pass_marks_pra"));
                model.setPass_marks_sub(subject_info.getJSONObject(i).optString("pass_marks_sub"));
                model.setShort_name(subject_info.getJSONObject(i).optString("short_name"));
                model.setSl(subject_info.getJSONObject(i).optString("sl"));
                model.setSub_part_id(subject_info.getJSONObject(i).optString("sub_part_id"));
                model.setSubject_id(subject_info.getJSONObject(i).optString("subject_id"));
                model.setSubject_name(subject_info.getJSONObject(i).optString("subject_name"));
                model.setTotal_marks(subject_info.getJSONObject(i).optString("total_marks"));

                all_subject_array.add(subject_info.getJSONObject(i).optString("subject_name"));
                all_subject_list.add(model);
            }


            setvGeneralvalues();

            adapter.notifyDataSetChanged();


            populateAttendence();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateAttendence() {

        ArrayAdapter shift_adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, HomeFragment.all_shift_array);
        ArrayAdapter class_adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, HomeFragment.all_class_array);
        ArrayAdapter section_adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, HomeFragment.all_section_array);


        shift_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        section_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        sp_shift.setAdapter(shift_adapter);
        sp_class.setAdapter(class_adapter);
        sp_section.setAdapter(section_adapter);

    }

    private void setvGeneralvalues() {
        schoolName.setText(company_name);
        phoneNumber.setText("Phone Number : " + phone);
        mobileNumber.setText("Mobile Number :  " + mobile);
        eMail.setText("Email : " + email);

    }

    @Override
    public void onPause() {
        super.onPause();

    }
}




package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.studentModel;

import java.util.ArrayList;


public class AttendenceAdapter extends RecyclerView.Adapter<AttendenceAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<studentModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public AttendenceAdapter(ArrayList<studentModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_attendence, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint({"NewApi", "RecyclerView"})
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        String title = list_of_title.get(position).getStudent_name();
        String roll = list_of_title.get(position).getRoll_no();

        holder.attendence_name.setText(title);
        holder.attendence_roll.setText(roll);
        holder.attendence_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                addAttendence.onAttendence(position,compoundButton,holder,isChecked);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list_of_title.size();
    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        AttendenceAdapter.setRecyclerClick = setRecyclerClick;
    }

    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView attendence_name, attendence_roll;

        SwitchCompat attendence_switch;

        public MyViewHolder(View view) {
            super(view);
            attendence_name = (TextView) view.findViewById(R.id.attendence_name);
            attendence_roll = (TextView) view.findViewById(R.id.attendence_roll);
            attendence_switch = (SwitchCompat) view.findViewById(R.id.attendence_switch);

        }
    }


    private static AddAttendence addAttendence;

    public interface AddAttendence {

        void onAttendence(int position, View view, MyViewHolder holder, boolean isChecked);
    }


    public void setGroupSMS(AddAttendence group) {

        addAttendence = group;
    }


}

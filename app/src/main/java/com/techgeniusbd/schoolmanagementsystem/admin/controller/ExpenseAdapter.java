package com.techgeniusbd.schoolmanagementsystem.admin.controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techgeniusbd.schoolmanagementsystem.R;
import com.techgeniusbd.schoolmanagementsystem.admin.model.admin.expenseItemModel;

import java.util.ArrayList;


public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.MyViewHolder> {
    private static RecyclerClick setRecyclerClick;
    Context context;
    int selected_position = 0;
    private ArrayList<expenseItemModel> list_of_title;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();
    int i = 0;


    public ExpenseAdapter(ArrayList<expenseItemModel> list_of_title, Context context, int i) {
        this.list_of_title = list_of_title;
        this.context = context;
        this.i = i;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_cell_expense_, parent, false);


        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String expense_sector = list_of_title.get(position).getExpense_type_name();
        String expense_date = list_of_title.get(position).getDate();
        String expense_amount = list_of_title.get(position).getAmount();

       holder.expense_sector.setText(expense_sector);
       holder.expense_date.setText(expense_date);
       holder.expense_amount.setText(expense_amount+ "/= ");

    }

    @Override
    public int getItemCount() {
       return list_of_title.size();

    }

    public void setOnItemClickListener(RecyclerClick setRecyclerClick) {
        ExpenseAdapter.setRecyclerClick = setRecyclerClick;
    }


    public interface RecyclerClick {
        void OnItemClick(int position, View view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView expense_amount,expense_date,expense_sector;

        public MyViewHolder(View view) {
            super(view);
            expense_amount = (TextView) view.findViewById(R.id.expense_amount);
            expense_date = (TextView) view.findViewById(R.id.expense_date);
            expense_sector = (TextView) view.findViewById(R.id.expense_sector);


        }
    }

}
